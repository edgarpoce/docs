---
title: "Add-on descriptor"
platform: cloud
product: jswcloud
category: devguide
subcategory: blocks
date: "2016-11-02"
---

{{< include path="docs/content/cloud/jira/platform/temp/add-on-descriptor.snippet.md" >}}

{{< include path="docs/content/cloud/connect/reference/descriptor-schemas.snippet.md">}}

{{< include path="docs/content/cloud/jira/platform/temp/add-on-descriptor-reference.snippet.md" >}}

{{< include path="docs/content/cloud/jira/platform/temp/authentication.snippet.md" >}}

{{< include path="docs/content/cloud/jira/platform/temp/lifecycle.snippet.md" >}}

{{< include path="docs/content/cloud/jira/platform/temp/add-on-vendor.snippet.md" >}}