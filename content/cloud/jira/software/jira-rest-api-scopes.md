---
title: JIRA cloud platform REST API scopes
platform: cloud
product: jswcloud
category: reference
subcategory: api
date: "2016-11-02"
type: scopes
scopeskey: jira
---
# JIRA cloud platform REST API scopes

{{< include path="docs/content/cloud/connect/reference/product-api-scopes.snippet.md" >}}