---
title: JIRA Software REST API scopes
platform: cloud
product: jswcloud
category: reference
subcategory: api
date: "2016-11-02"
type: scopes
scopeskey: jiraSoftware
---
# JIRA Software REST API scopes

{{< include path="docs/content/cloud/connect/reference/product-api-scopes.snippet.md" >}}