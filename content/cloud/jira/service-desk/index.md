---
title: Latest updates for JIRA Service Desk
platform: cloud
product: jsdcloud
category: devguide
subcategory: index
date: "2017-01-18"
---

# Latest updates

We deploy updates to JIRA Service Desk Cloud frequently. As a JIRA developer, it's important that you're aware of the changes. The resources below will help you keep track of what's happening.

## Recent changes

#### Request create property panel

We have added support for defining your own [request create property panels](request-create-property-panel) which
are displayed on the request creation screen in the customer portal and enable add-ons to save arbitrary data during
request creation as JIRA issue properties.

#### Issue fields support

Single and multi select [issue fields](modules/issue-field) supplied by Connect add-ons are now supported in JSD customer portal.

#### Non-experimental APIs

We have removed the experimental flag from the following APIs:

* `/servicedeskapi/organization`
* `/servicedeskapi/servicedesk/{serviceDeskId}/organization`
* `/servicedeskapi/servicedesk/{serviceDeskId}/queue`
* `/servicedeskapi/servicedesk/{serviceDeskId}/requesttypegroup`

#### New REST APIs

We have added the following APIs:

* `/servicedeskapi/customer`
* `/servicedeskapi/request/{issueIdOrKey}/approval`
* `/servicedeskapi/request/{issueIdOrKey}/transition`
* `/servicedeskapi/servicedesk/{serviceDeskId}/customer`

For up-to-date and complete docs, see the [JSD REST API documentation](https://docs.atlassian.com/jira-servicedesk/REST/cloud/).

#### Platform
Also see the [latest updates for the JIRA Platform](../platform).

## Atlassian Developer blog

Major changes that affect JIRA Cloud developers are announced in the **Atlassian Developer blog**, like new JIRA modules or the deprecation of API end points.
You'll also find handy tips and articles related to JIRA development.

Check it out and subscribe here: [Atlassian Developer blog](https://developer.atlassian.com/blog/)
 ([JIRA-related posts](https://developer.atlassian.com/blog/categories/jira/))

## Release notes

Changes to JIRA Service Desk Cloud are announced on the JIRA Service Desk blog.

Changes that affect all JIRA Cloud products are announced in the *What's New blog* for Atlassian Cloud. This includes new features, bug fixes, and other changes. For example, the introduction of a new JIRA quick search or a change in project navigation.

Check them out and subscribe here:  
[JIRA Service Desk blog](https://confluence.atlassian.com/pages/viewrecentblogposts.action?key=SERVICEDESKCLOUD)  
[What's new blog](https://confluence.atlassian.com/display/Cloud/What%27s+New) (Note, this blog also includes changes to other Cloud applications)
