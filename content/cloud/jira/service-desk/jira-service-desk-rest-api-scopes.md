---
title: JIRA Service Desk REST API scopes
platform: cloud
product: jsdcloud
category: reference
subcategory: api
date: "2016-10-31"
type: scopes
scopeskey: jiraServiceDesk
---
# JIRA Service Desk REST API scopes

{{< include path="docs/content/cloud/connect/reference/product-api-scopes.snippet.md" >}}