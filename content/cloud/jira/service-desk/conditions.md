---
title: Conditions
platform: cloud
product: jsdcloud
category: devguide
subcategory: blocks
aliases:
    - /jiracloud/conditions.html
    - /jiracloud/conditions.md
    - /cloud/jira/service-desk/conditions.html
    - /cloud/jira/service-desk/conditions.html
title: Conditions
date: "2016-10-31"
---
{{< include path="docs/content/cloud/connect/concepts/jira-conditions.snippet.md">}}