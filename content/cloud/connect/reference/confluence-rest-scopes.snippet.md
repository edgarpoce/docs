# Confluence REST scopes
For more information about the Confluence REST APIs, please consult the documentation on [developer.atlassian.com](https://developer.atlassian.com/display/CONFDEV/Confluence+REST+APIs+-+Prototype+Only).

{{< include path="docs/content/cloud/connect/reference/product-api-scopes.snippet.md" >}}