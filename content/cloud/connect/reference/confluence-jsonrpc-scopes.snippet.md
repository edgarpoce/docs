# Confluence JSON-RPC scopes
For more information about the Confluence JSON-RPC APIs, please consult the documentation on [developer.atlassian.com](https://developer.atlassian.com/display/CONFDEV/Confluence+JSON-RPC+APIs).
Both version 1 and 2 of JSON-RPC are supported, i.e. RPC calls to the following endpoints are covered:
 *   ``/confluenceservice-v1``
 *   ``/confluenceservice-v2``

The required scope for your add-on depends on the methods that your add-on invokes. 
The table below shows the required scope for every RPC method call.